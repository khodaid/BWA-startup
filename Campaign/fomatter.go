package campaign

type CampaignFormater struct {
	Id               int    `json:"id"`
	UserId           int    `json:"user_id"`
	Name             string `json:"name"`
	ShortDescription string `json:"short_description"`
	GoalAmount       int    `json:"goal_amount"`
	CurrentAmount    int    `json:"current_amount"`
	ImageUrl         string `json:"image_url"`
}

func FormatterCampaign(campaign Campaign) CampaignFormater {
	formater := CampaignFormater{
		Id:               campaign.Id,
		UserId:           campaign.UserId,
		Name:             campaign.Name,
		ShortDescription: campaign.ShortDescription,
		GoalAmount:       campaign.GoalAmount,
		CurrentAmount:    campaign.CurrentAmount,
		ImageUrl:         "",
	}

	if len(campaign.CampaignImages) > 0 {
		formater.ImageUrl = campaign.CampaignImages[0].FileName
	}

	return formater
}

func FormatterCampaigns(campaigns []Campaign) []CampaignFormater {
	var campaignsFormatter []CampaignFormater

	for _, campaign := range campaigns {
		campaignFormatter := FormatterCampaign(campaign)
		campaignsFormatter = append(campaignsFormatter, campaignFormatter)
	}
	return campaignsFormatter
}
