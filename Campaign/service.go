package campaign

type Service interface {
	FindCampaigns(id int) ([]Campaign, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindCampaigns(id int) ([]Campaign, error) {
	if id != 0 {
		campaigns, err := s.repository.FindById(id)
		if err != nil {
			return nil, err
		}
		return campaigns, nil
	}
	campaigns, err := s.repository.FindAll()
	if err != nil {
		return nil, err
	}
	return campaigns, nil

}
