package Handler

import (
	Campaign "bwa_startup/Campaign"
	"bwa_startup/Helper"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// tangkap parameter dihandler
// handler ke service
// service yang menentukan repository mana yang di panggil
// repository akses ke DB

type campaignHandler struct {
	service Campaign.Service
}

func NewCampaignHandler(service Campaign.Service) *campaignHandler {
	return &campaignHandler{service}
}

func (h *campaignHandler) GetCampaigns(c *gin.Context) {
	userID, _ := strconv.Atoi(c.Query("user_id"))

	campaigns, err := h.service.FindCampaigns(userID)
	if err != nil {
		respone := Helper.APIRespone("Error get campaign", http.StatusBadRequest, "errors", Campaign.FormatterCampaigns(campaigns))
		c.JSON(http.StatusBadRequest, respone)
		return
	}

	respone := Helper.APIRespone("Success", http.StatusOK, "Success", Campaign.FormatterCampaigns(campaigns))
	c.JSON(http.StatusOK, respone)
}
