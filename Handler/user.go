package Handler

import (
	"bwa_startup/Auth"
	"bwa_startup/Helper"
	"bwa_startup/User"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	userService User.Service
	authService Auth.Service
}

func NewUserHandler(userService User.Service, authService Auth.Service) *userHandler {
	return &userHandler{userService, authService}
}

func (h *userHandler) RegisterUser(c *gin.Context) {
	// tangkap input dari user
	// map input dari user ke struct RegisterUserInput
	// struct diatas kita passing sebagai parameter service
	var input User.RegisterUserInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := Helper.FormatValidatorError(err)
		errorMessage := gin.H{"error": errors}

		respone := Helper.APIRespone("Register account failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, respone)
		return
	}

	user, err := h.userService.RegisterUser(input)

	if err != nil {
		respone := Helper.APIRespone("Register account failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, respone)
		return
	}

	token, err := h.authService.GenerateToken(user.ID)

	if err != nil {
		respone := Helper.APIRespone("Register account failed", http.StatusBadRequest, "error", err)
		c.JSON(http.StatusBadRequest, respone)
		return
	}

	formatter := User.FormatterUser(user, token)
	respone := Helper.APIRespone("Accoount has been registered", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, respone)
}

func (h *userHandler) Login(c *gin.Context) {
	// user memasukan input
	// input ditangkap handle
	// mapping dari user ke input struct
	// input struct passing service
	// di service mencari dengan bantuan repository user dengan email X
	// mecocokan password

	var input User.LoginInput

	err := c.ShouldBindJSON(&input)

	if err != nil {
		errors := Helper.FormatValidatorError(err)
		errorMessage := gin.H{"error": errors}

		respone := Helper.APIRespone("Login Failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, respone)
		return
	}

	loggedInUser, err := h.userService.Login(input)

	if err != nil {
		errors := Helper.FormatValidatorError(err)
		errorMessage := gin.H{"error": errors}

		respone := Helper.APIRespone("Login Failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, respone)
		return
	}

	token, err := h.authService.GenerateToken(loggedInUser.ID)

	if err != nil {
		respone := Helper.APIRespone("Register account failed", http.StatusBadRequest, "error", err)
		c.JSON(http.StatusBadRequest, respone)
		return
	}

	formater := User.FormatterUser(loggedInUser, token)
	respone := Helper.APIRespone("Successfully Loggedin", http.StatusOK, "success", formater)

	c.JSON(http.StatusOK, respone)

}

func (h *userHandler) CheckEmailAvailability(c *gin.Context) {
	// ada input email dari user
	// input email di mapping ke struct input
	// struct input di passing ke service
	// service akan memanggil repository apakah email sudah ada apa belum
	// repository akan query ke DB

	var user User.CheckEmailInput

	err := c.ShouldBindJSON(&user)

	if err != nil {
		errors := Helper.FormatValidatorError(err)
		errorMessage := gin.H{"error": errors}

		respone := Helper.APIRespone("Email checking failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, respone)
		return
	}

	isEmailAvailable, err := h.userService.IsEmailAvalaible(user)

	if err != nil {
		errorMessage := gin.H{"error": "Server error"}

		respone := Helper.APIRespone("Email checking failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, respone)
		return
	}

	data := gin.H{
		"is_available": isEmailAvailable,
	}

	metaMessage := "Email has been registered"

	if isEmailAvailable {
		metaMessage = "Email is available"
	}

	respone := Helper.APIRespone(metaMessage, http.StatusOK, "success", data)
	c.JSON(http.StatusOK, respone)
}

func (h *userHandler) UploadAvatar(c *gin.Context) {
	// input avatar
	// simpan gambar di folder
	// diservice kita panggil repository
	// JWT (sementara hard code. contoh user login id = 1)
	// repo ambil data user.id = 1
	// repo update data user simpan lokasi file

	file, err := c.FormFile("avatar")

	if err != nil {
		errorMessage := gin.H{"is_uploaded": false}
		respone := Helper.APIRespone("Failed to upload avatar image", http.StatusBadRequest, "error", errorMessage)

		c.JSON(http.StatusBadRequest, respone)
		return
	}

	currentUser := c.MustGet("currentUser").(User.User)
	userId := currentUser.ID

	path := fmt.Sprintf("images/%d-%s", userId, file.Filename)
	err = c.SaveUploadedFile(file, path)

	if err != nil {
		errorMessage := gin.H{"is_uploaded": false}
		respone := Helper.APIRespone("Failed to upload avatar image", http.StatusBadRequest, "error", errorMessage)

		c.JSON(http.StatusBadRequest, respone)
		return
	}

	_, err = h.userService.SaveAvatar(userId, path)

	if err != nil {
		errorMessage := gin.H{"is_uploaded": false}
		respone := Helper.APIRespone("Failed to upload avatar image", http.StatusBadRequest, "error", errorMessage)

		c.JSON(http.StatusBadRequest, respone)
		return
	}

	errorMessage := gin.H{"is_uploded": true}
	respone := Helper.APIRespone("Avatar successfuly uploaded", http.StatusOK, "success", errorMessage)

	c.JSON(http.StatusOK, respone)
}
