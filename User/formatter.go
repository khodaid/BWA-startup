package User

type UserFormatter struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Token string `json:"token"`
}

func FormatterUser(user User, token string) UserFormatter {
	formatter := UserFormatter{
		Id:    user.ID,
		Name:  user.Name,
		Email: user.Email,
		Token: token,
	}

	return formatter
}
