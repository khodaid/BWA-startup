package User

type RegisterUserInput struct {
	Name       string `json:"name" binding:"required"`
	Occupation string `json:"occupation" binding:"required"`
	Email      string `json:"email" binding:"required,email"`
	Password   string `json:"password" binding:"required"`
	Role       string
}

type LoginInput struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password"`
}

type CheckEmailInput struct {
	Email string `json:"email" binding:"required,email"`
}
