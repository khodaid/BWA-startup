package main

import (
	"bwa_startup/Auth"
	campaign "bwa_startup/Campaign"
	"bwa_startup/Handler"
	"bwa_startup/Helper"
	"bwa_startup/User"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/bwa-startup?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Connection to database success")

	userRepository := User.NewRepository(db)
	userService := User.NewService(userRepository)
	authService := Auth.NewService()
	userHandle := Handler.NewUserHandler(userService, authService)

	campaignRepository := campaign.NewRepository(db)
	campaignService := campaign.NewService(campaignRepository)
	campaignHandler := Handler.NewCampaignHandler(campaignService)

	router := gin.Default()

	api := router.Group("api/v1")

	api.POST("/user", userHandle.RegisterUser)
	api.POST("/session", userHandle.Login)
	api.POST("/email_checkers", userHandle.CheckEmailAvailability)
	api.POST("/avatars", authMiddleware(authService, userService), userHandle.UploadAvatar)

	api.GET("/campaigns", campaignHandler.GetCampaigns)

	router.Run()

}

func authMiddleware(authService Auth.Service, userService User.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHandler := c.GetHeader("Authorization")

		if !strings.Contains(authHandler, "Bearer") {
			respone := Helper.APIRespone("Unathorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, respone)
			return
		}

		tokenString := ""

		arrayToken := strings.Split(authHandler, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)

		if err != nil {
			respone := Helper.APIRespone("Unathorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, respone)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			respone := Helper.APIRespone("Unathorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, respone)
			return
		}

		userId := int(claim["user_id"].(float64))

		user, err := userService.GetUserById(userId)
		if err != nil {
			respone := Helper.APIRespone("Unathorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, respone)
			return
		}

		c.Set("currentUser", user)
	}
}

// ambil nilai header Authorization : Bearer tokentokentoken
// dari header Authorization, kita ambil nilai tokennya saja
// kita validasi token
// kita ambil user_id
// ambil dari DB berdasarkan user_id lewat service
// kita set context isinya user
